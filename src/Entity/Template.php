<?php

namespace App\Entity;

use App\Repository\TemplateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TemplateRepository::class)]
class Template
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $template_name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $template_color = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $sec_color = null;

    #[ORM\ManyToOne(inversedBy: 'templates')]
    private ?User $user = null;

    #[ORM\OneToMany(targetEntity: Theme::class, mappedBy: 'template')]
    private Collection $themes;

    public function __toString()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->themes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getTemplateName(): ?string
    {
        return $this->template_name;
    }

    public function setTemplateName(?string $template_name): static
    {
        $this->template_name = $template_name;

        return $this;
    }

    public function getTemplateColor(): ?string
    {
        return $this->template_color;
    }

    public function setTemplateColor(?string $template_color): static
    {
        $this->template_color = $template_color;

        return $this;
    }

    public function getSecColor(): ?string
    {
        return $this->sec_color;
    }

    public function setSecColor(?string $sec_color): static
    {
        $this->sec_color = $sec_color;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Theme>
     */
    public function getThemes(): Collection
    {
        return $this->themes;
    }

    public function addTheme(Theme $theme): static
    {
        if (!$this->themes->contains($theme)) {
            $this->themes->add($theme);
            $theme->setTemplate($this);
        }

        return $this;
    }

    public function removeTheme(Theme $theme): static
    {
        if ($this->themes->removeElement($theme)) {
            // set the owning side to null (unless already changed)
            if ($theme->getTemplate() === $this) {
                $theme->setTemplate(null);
            }
        }

        return $this;
    }
}
