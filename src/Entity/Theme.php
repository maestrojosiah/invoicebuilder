<?php

namespace App\Entity;

use App\Repository\ThemeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ThemeRepository::class)]
class Theme
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $pri_color = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $sec_color = null;

    #[ORM\ManyToOne(inversedBy: 'themes')]
    private ?User $user = null;

    #[ORM\ManyToOne(inversedBy: 'themes')]
    private ?Template $template = null;

    #[ORM\OneToMany(targetEntity: Invoice::class, mappedBy: 'theme')]
    private Collection $invoices;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    public function __construct()
    {
        $this->invoices = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPriColor(): ?string
    {
        return $this->pri_color;
    }

    public function setPriColor(?string $pri_color): static
    {
        $this->pri_color = $pri_color;

        return $this;
    }

    public function getSecColor(): ?string
    {
        return $this->sec_color;
    }

    public function setSecColor(?string $sec_color): static
    {
        $this->sec_color = $sec_color;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getTemplate(): ?Template
    {
        return $this->template;
    }

    public function setTemplate(?Template $template): static
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return Collection<int, Invoice>
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): static
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices->add($invoice);
            $invoice->setTheme($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): static
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getTheme() === $this) {
                $invoice->setTheme(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }
}
