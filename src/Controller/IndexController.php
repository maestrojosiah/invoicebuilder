<?php

namespace App\Controller;

use App\Repository\InvoiceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'app_index')]
    public function index(): Response
    {
        return $this->render('index/index.html.twig', [
            'test' => 'test',
        ]);
    }

    #[Route('/invoice/generator', name: 'invoice_generator')]
    public function invoice_generator(InvoiceRepository $invoiceRepo, Request $request): Response
    {
        $invoiceNumber = $request->query->get('i-n');
        $invoice = $invoiceRepo->findOneByInvoiceNumber($invoiceNumber);
        if (!$invoice) {
            $invoice = $invoiceRepo->findFirstEntry();
        }
        $template = $invoice->getTheme()->getTemplate();
        return $this->render('index/' . $template->getTemplateName(), [
            'invoice' => $invoice,
        ]);
    }
}
