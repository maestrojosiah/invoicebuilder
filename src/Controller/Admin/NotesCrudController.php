<?php

namespace App\Controller\Admin;

use App\Entity\Notes;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;

class NotesCrudController extends AbstractCrudController
{
    public function __construct( private EntityRepository $entityManager)
    {}

    public static function getEntityFqcn(): string
    {
        return Notes::class;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = $this->entityManager->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        if (in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
            $qb->andWhere('entity.user = :user');
        } else {
            $qb->andWhere('entity.user = :user');
        }

        $qb->setParameter('user', $this->getUser());

        return $qb;

        // return $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
    }

   public function createEntity(string $entityFqcn)
    {
        $template = new Notes();
        $template->setUser($this->getUser());

        return $template;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setUser($this->getUser());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }


    public function configureFields(string $pageName): iterable
    {
        yield TextareaField::new("description")->setLabel("Description");
        yield AssociationField::new("user")->setLabel("User")
        ->onlyOnDetail();

    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setEntityLabelInSingular("Note");
    }

}
