<?php

namespace App\Controller\Admin;

use App\Entity\Client;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class ClientCrudController extends AbstractCrudController
{
    public function __construct( private EntityRepository $entityManager, private AdminUrlGenerator $adminUrlGenerator)
    {}

    public static function getEntityFqcn(): string
    {
        return Client::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $client = new Client();
        $client->setUser($this->getUser());

        return $client;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = $this->entityManager->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        if (in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
            $qb->andWhere('entity.user = :user');
        } else {
            $qb->andWhere('entity.user = :user');
        }

        $qb->setParameter('user', $this->getUser());

        return $qb;

        // return $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
    }

    
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setUser($this->getUser());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function configureFields(string $pageName): iterable
    {

        yield TextField::new("name")->setLabel("Client Name"); 
        yield TextField::new("contactDetails")->setLabel("Contact Details");
        yield TextField::new("address")->setLabel("Address")
            ->hideOnIndex();
        yield TextField::new("email")->setLabel("Email");
        yield TextField::new("phone")->setLabel("Phone");
        yield AssociationField::new("company")->setLabel("Company")
            ->hideOnIndex();
        yield AssociationField::new("user")->setLabel("User")
            ->onlyOnDetail();

    }

    public function configureActions(Actions $actions): Actions
    {

        $templatesUrl = $this->adminUrlGenerator
            ->setController(TemplateCrudController::class)
            ->setAction(Crud::PAGE_INDEX)
            ->generateUrl();

        // if the method is not defined in a CRUD controller, link to its route
        $sendInvoice = Action::new('goToTemplates', 'Templates', 'fa fa-arrow-right')
            ->addCssClass('btn btn-info')
            ->linkToUrl($templatesUrl);

        return $actions
        // ...
        ->add(Crud::PAGE_INDEX, $sendInvoice)
        // ->remove(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER)
        ;
        
    }
    

}
