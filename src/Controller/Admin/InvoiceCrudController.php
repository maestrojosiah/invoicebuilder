<?php

namespace App\Controller\Admin;

use App\Entity\Invoice;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class InvoiceCrudController extends AbstractCrudController
{
    public function __construct( private EntityRepository $entityManager, private AdminUrlGenerator $adminUrlGenerator)
    {}

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = $this->entityManager->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        if (in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
            $qb->andWhere('entity.user = :user');
        } else {
            $qb->andWhere('entity.user = :user');
        }

        $qb->setParameter('user', $this->getUser());

        return $qb;

        // return $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
    }

    public static function getEntityFqcn(): string
    {
        return Invoice::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new Invoice();
        $template->setUser($this->getUser());
        $template->setType('invoice');
        $template->setPages('single_page');
        $template->setTaxType('inclusive');

        return $template;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setUser($this->getUser());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function configureFields(string $pageName): iterable
    {
        // yield FormField::addTab("Invoice Details");
        yield FormField::addColumn(6)->setLabel("Basic Info");

        yield IntegerField::new("totalAmount")->setLabel("Total Invoice Amount");
        yield TextField::new("invoiceNumber")->setLabel("Invoice Number")
            ->setTemplatePath('admin/fields/link.html.twig');        
        yield DateField::new("date")->setLabel("Date")
            ->hideOnIndex();
        yield DateField::new("dueDate")->setLabel("Due Date")
            ->hideOnIndex()
        ->setHelp("For invoices only, leave blank if you're preparing a receipt");
        yield AssociationField::new("theme")->setLabel("Invoice Theme");
        yield AssociationField::new("client")->setLabel("Client")
            ->hideOnIndex();
        yield AssociationField::new("user")->setLabel("User")
        ->onlyOnDetail()
            ->hideOnIndex();
        yield ChoiceField::new("type")->setLabel("Type")
            ->setChoices([
                'Invoice' => 'invoice',
                'Receipt' => 'receipt',
            ]);
        yield ChoiceField::new("pages")->setLabel("Page Settings")
            ->setChoices([
                'Single Page' => 'single_page',
                'Multi-Page' => 'multipage',
            ])
            ->renderAsBadges()
            ->renderExpanded()
            ->setHelp("Select multi-page if your invoice has more than 6 items. Change this setting if your invoice download will cut out some items at the bottom");

        yield FormField::addColumn(6)->setLabel("More Details");
        yield TextareaField::new("paymentInfo")->setLabel("Payment Info")
            ->hideOnIndex()
            ->setHelp("For invoices only, leave blank if you're preparing a receipt");
        yield AssociationField::new("notes")->setLabel("Notes")
            ->hideOnIndex();
        yield TextAreaField::new("termsConditions")->setLabel("Terms & Conditions")
            ->hideOnIndex();

        // yield FormField::addTab("Amount & Tax Details");
        // yield FormField::addColumn(6)->setLabel("Payment Details");
        
        yield IntegerField::new("tax")->setLabel("Tax Amount")
            ->hideOnIndex();
        yield IntegerField::new("discount")->setLabel("Discount Amount")
            ->hideOnIndex();
        yield ChoiceField::new("taxType")->setLabel("Tax Type")
            ->setChoices([
                'Inclusive' => 'inclusive',
                'Exclusive' => 'exclusive',
            ])
                ->hideOnIndex()
            ->setHelp("Is the tax included in the figure or should it be added to the invoice figure?");
    }


    public function configureActions(Actions $actions): Actions
    {

        $invoiceItemUrl = $this->adminUrlGenerator
            ->setController(InvoiceItemCrudController::class)
            ->setAction(Crud::PAGE_NEW)
            ->generateUrl();

        // if the method is not defined in a CRUD controller, link to its route
        $sendInvoice = Action::new('addItems', 'Add Items', 'fa fa-arrow-right')
            ->addCssClass('btn btn-info')
            ->linkToUrl($invoiceItemUrl);

        return $actions
        // ...
        ->add(Crud::PAGE_INDEX, $sendInvoice)
        // ->remove(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER)
        ;
        
    }

}
