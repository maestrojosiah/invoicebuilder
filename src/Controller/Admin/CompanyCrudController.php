<?php

namespace App\Controller\Admin;

use App\Entity\Company;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class CompanyCrudController extends AbstractCrudController
{

    public function __construct( private EntityRepository $entityManager, private AdminUrlGenerator $adminUrlGenerator)
    {}

    public static function getEntityFqcn(): string
    {
        return Company::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new Company();
        $template->setUser($this->getUser());

        return $template;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = $this->entityManager->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        if (in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
            $qb->andWhere('entity.user = :user');
        } else {
            $qb->andWhere('entity.user = :user');
        }

        $qb->setParameter('user', $this->getUser());

        return $qb;

        // return $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setUser($this->getUser());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function configureFields(string $pageName): iterable
    {

        yield TextField::new("companyName")->setLabel("Company Name"); 
        yield ImageField::new("logoPath")->setLabel("Logo")
        ->setUploadDir("/public/invoices/images")
        ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
        ->setBasePath("/invoices/images");
        yield ChoiceField::new("logoType")->setLabel("Logo Type")
            ->setChoices([
                'Square/Near Square [max-width:150px]' => 'square',
                'Long [max-width:300px]' => 'long',
            ]);
        yield TextField::new("address")->setLabel("Address");
        yield TextField::new("email")->setLabel("Email");
        yield TextField::new("phone")->setLabel("Phone");
        yield TextField::new("website")->setLabel("Website");
        yield AssociationField::new("user")->setLabel("User")
            ->onlyOnDetail();


    }

    public function configureActions(Actions $actions): Actions
    {

        $clientUrl = $this->adminUrlGenerator
            ->setController(ClientCrudController::class)
            ->setAction(Crud::PAGE_NEW)
            ->generateUrl();

        // if the method is not defined in a CRUD controller, link to its route
        $sendInvoice = Action::new('addClient', 'Add Client', 'fa fa-arrow-right')
            ->addCssClass('btn btn-info')
            ->linkToUrl($clientUrl);

        return $actions
        // ...
        ->add(Crud::PAGE_INDEX, $sendInvoice)
        // ->remove(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER)
        ;
        
    }

}
