<?php

namespace App\Controller\Admin;

use App\Entity\Theme;
use App\Repository\TemplateRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\RequestStack;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;

class ThemeCrudController extends AbstractCrudController
{

    public function __construct(private RequestStack $requestStack, private TemplateRepository $templateRepo, private EntityRepository $entityManager){}
    public static function getEntityFqcn(): string
    {
        return Theme::class;
    }


    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplate('crud/new', 'admin/theme/form.html.twig');
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = $this->entityManager->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        if (in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
            $qb->andWhere('entity.user = :user');
        } else {
            $qb->andWhere('entity.user = :user');
        }

        $qb->setParameter('user', $this->getUser());

        return $qb;

        // return $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
    }


    public function createEntity(string $entityFqcn) {
        $theme = new Theme();
        $theme->setUser($this->getUser());

        $templateValue = $this->getTemplateValueFromRequest();

        $template = $this->templateRepo->find((int)$templateValue);
        $theme->setTemplate($template);
        return $theme;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setUser($this->getUser());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }


    public function configureFields(string $pageName): iterable
    {
        $templateValue = $this->getTemplateValueFromRequest();

        yield AssociationField::new("template")->setLabel("Template")
            // ->hideOnIndex()
            // ->setDisabled($this->getTemplateValueFromRequest() !== null)
            ->setHelp("<span style='color:#FF6E35;'>If you change this, allow page to reload.</span>");
        yield TextField::new("name")->setLabel("Template Name");
            // ->showValue();
        yield ColorField::new("pri_color")->setLabel("Template Primary Color")
            ->showValue();
        yield ColorField::new("sec_color")->setLabel("Template Secondary Color")
            ->showValue();
        yield AssociationField::new("user")->setLabel("User")
            ->onlyOnDetail();
        // ->setCustomOption('template_value', $templateValue); // pass the dynamic value
    }

    // public function configureActions(Actions $actions): Actions
    // {

    //     // if the method is not defined in a CRUD controller, link to its route
    //     $setTheme = Action::new('showTemplate', 'Set Theme Colors', 'fa fa-paint-brush')
    //         ->addCssClass('btn btn-info')
    //         ->linkToCrudAction(Crud::PAGE_INDEX)
    //         ->setTemplatePath('admin/views/crud/new.html.twig');

    //     return $actions
    //     ->add(Crud::PAGE_NEW, $setTheme)
    //     ;
    // }

    private function getTemplateValueFromRequest(): ?int
    {
        // Get the 'tmp' parameter from the request
        $tmpValue = $this->requestStack->getCurrentRequest()->query->get("tmp");


        // You may need to validate and sanitize the input based on your application logic
        // Here, I'm assuming 'tmp' is an integer, adjust as needed
        // var_dump($tmpValue);
        return $tmpValue;
    }


}
