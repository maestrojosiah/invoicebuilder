<?php

namespace App\Controller\Admin;

use App\Entity\Client;
use App\Entity\Company;
use App\Entity\Invoice;
use App\Entity\InvoiceItem;
use App\Entity\Notes;
use App\Entity\Template;
use App\Entity\Theme;
use App\Entity\User;
use App\Repository\InvoiceRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class DashboardController extends AbstractDashboardController
{

    public function __construct(private InvoiceRepository $invoiceRepository, private ChartBuilderInterface $chartBuilder, private AdminUrlGenerator $adminUrlGenerator){}

    #[Route('/user', name: 'admin')]
    public function index(): Response
    {

        $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);

        $chart->setData([
            'labels' => ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            'datasets' => [
                [
                    'label' => 'My First dataset',
                    'backgroundColor' => 'rgb(255, 99, 132)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => [0, 10, 5, 2, 20, 30, 45],
                ],
            ],
        ]);

        $chart->setOptions([
            'scales' => [
                'y' => [
                    'suggestedMin' => 0,
                    'suggestedMax' => 100,
                ],
            ],
        ]);

        $invoices = $this->invoiceRepository->findBy(
            ['user' => $this->getUser()],
            ['id'=> 'DESC']
        );

        $companyUrl = $this->adminUrlGenerator
            ->setController(CompanyCrudController::class)
            ->setAction(Crud::PAGE_NEW)
            ->generateUrl();
        $clientUrl = $this->adminUrlGenerator
            ->setController(ClientCrudController::class)
            ->setAction(Crud::PAGE_NEW)
            ->generateUrl();
        $templatesUrl = $this->adminUrlGenerator
            ->setController(TemplateCrudController::class)
            ->setAction(Crud::PAGE_INDEX)
            ->generateUrl();
        $invoiceUrl = $this->adminUrlGenerator
            ->setController(InvoiceCrudController::class)
            ->setAction(Crud::PAGE_NEW)
            ->generateUrl();
        $invoiceItemUrl = $this->adminUrlGenerator
            ->setController(InvoiceItemCrudController::class)
            ->setAction(Crud::PAGE_NEW)
            ->generateUrl();
        $notesUrl = $this->adminUrlGenerator
            ->setController(NotesCrudController::class)
            ->setAction(Crud::PAGE_NEW)
            ->generateUrl();
        $urls = ['companyUrl' => $companyUrl, 'clientUrl' => $clientUrl, 'templatesUrl' => $templatesUrl, 'invoiceUrl' => $invoiceUrl, 'invoiceItemUrl' => $invoiceItemUrl, 'notesUrl' => $notesUrl];

        return $this->render('admin/index.html.twig', [
            'invoices' => $invoices,
            'chart' => $chart,
            'urls' => $urls
        ]);

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureActions(): Actions
    {
        return parent::configureActions()
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Invoice Builder')
            ->setFaviconPath('/site/favicon.ico');
            
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-dashboard');
        yield MenuItem::linkToCrud('My Company', 'fas fa-building', Company::class);
        yield MenuItem::linkToCrud('My Clients', 'fas fa-user', Client::class);
        yield MenuItem::linkToCrud('Templates', 'fas fa-file', Template::class);
        yield MenuItem::linkToCrud('My Themes', 'fas fa-paint-brush', Theme::class);
        yield MenuItem::linkToCrud('My Notes', 'fas fa-pen', Notes::class);
        yield MenuItem::linkToCrud('My Invoices', 'fas fa-file-invoice', Invoice::class);
        yield MenuItem::linkToCrud('Invoice Items', 'fas fa-list-alt', InvoiceItem::class);
        yield MenuItem::linkToCrud('Users', 'fas fa-users', User::class)
            ->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToUrl('Homepage', 'fa fa-home', $this->generateUrl('app_index'));
    }

    public function configureCrud(): Crud
    {
        return parent::configureCrud()
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setPageTitle('index', '%entity_label_plural% listing')
            ->showEntityActionsInlined();
            // ->overrideTemplate('show.html.twig', 'admin/default/show.html.twig');
    }    

    

    public function configureAssets(): Assets
    {
        // return parent::configureAssets()
            // ->addCssFile('styles/app.css')
            // ->addJsFile('app.js')
            // ->addJsFile('vendor/chart.js/auto.js');
            // ->addJsFile('/invoices/js/jquery-1.9.1.js')
            // ->addJsFile('/invoices/js/spectrum.js')
            // ->addJsFile('/invoices/js/configSpectrum.js');
        $assets = parent::configureAssets();
        $assets->addAssetMapperEntry('app');
        return $assets;
    }

}
