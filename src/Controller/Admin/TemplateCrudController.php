<?php

namespace App\Controller\Admin;

use App\Entity\Client;
use App\Entity\Template;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class TemplateCrudController extends AbstractCrudController
{
    public function __construct(private AdminUrlGenerator $adminUrlGenerator){}
    public static function getEntityFqcn(): string
    {
        return Template::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new Template();
        $template->setUser($this->getUser());

        return $template;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setUser($this->getUser());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function configureFields(string $pageName): iterable
    {

        
        yield TextField::new("name")->setLabel("Template Name")
            ->setPermission('ROLE_ADMIN');
        yield TextField::new("template_name")->setLabel("Template Name")
            ->setTemplatePath('admin/fields/screenshot.html.twig');;
        yield ColorField::new("template_color")->setLabel("Template Theme Color")
            ->showValue();
        yield ColorField::new("sec_color")->setLabel("Template Secondary Color")
            ->showValue();
        yield AssociationField::new("user")->setLabel("User")
            ->onlyOnDetail();
            

    }

    public function configureActions(Actions $actions): Actions
    {

        $themeUrl = $this->adminUrlGenerator
            ->setController(ThemeCrudController::class)
            ->setAction(Crud::PAGE_NEW)
            // ->setAll(["tmp" => $this->getEntityId()])
            ->generateUrl();

        // if the method is not defined in a CRUD controller, link to its route
        $setTheme = Action::new('setTheme', 'Use This Template', 'fa fa-paint-brush')
            ->addCssClass('btn btn-info')
            ->linkToUrl($themeUrl)
            ->setTemplatePath('admin/fields/_action_set_theme.html.twig');

        return $actions
        // ...
        ->add(Crud::PAGE_INDEX, $setTheme)
        ->remove(Crud::PAGE_INDEX, Action::DELETE)
        ->remove(Crud::PAGE_INDEX, Action::DETAIL)
        ->remove(Crud::PAGE_INDEX, Action::EDIT)
        ;
    }
    
// add a button on each row / image - 'choose this' then redirect to invoice with the invoice selected
}
